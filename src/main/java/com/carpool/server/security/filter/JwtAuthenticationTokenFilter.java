package com.carpool.server.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.carpool.server.model.User;
import com.carpool.server.security.SecurityAppContext;
import com.carpool.server.security.factory.RememberMeAuthenticationTokenFactory;
import com.carpool.server.security.factory.UsernamePasswordAuthenticationTokenFactory;

import com.carpool.server.service.UserService;
import java.io.ByteArrayInputStream;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    static final String AUTHORIZATION = "Authorization";
    static final String SOURCE = "Source";
    static final String UTF_8 = "UTF-8";
    static final int BEGIN_INDEX = 7;
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    UserService userService;
    @Autowired
    UsernamePasswordAuthenticationTokenFactory usernamePasswordAuthenticationTokenFactory;
    @Autowired
    RememberMeAuthenticationTokenFactory rememberMeAuthenticationTokenFactory;
    @Autowired
    SecurityAppContext securityAppContext;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authToken = request.getHeader(AUTHORIZATION);
        String source = request.getHeader(SOURCE);
        if (authToken != null) {
            try {
                authToken = new String(authToken.substring(BEGIN_INDEX).getBytes(), UTF_8);
                SecurityContext context = securityAppContext.getContext();
                if (context.getAuthentication() == null) {
                    logger.info("Checking authentication for token " + authToken);
                    User u = userService.validateUser(authToken, request.getRemoteAddr(), source);
                    if (u != null) {
                        logger.info("User " + u.getUsername() + " found.");
                        Authentication authentication = null;
                        if (StringUtils.hasText(source) && source.equals(UserService.ADAL)) {
                            authentication = rememberMeAuthenticationTokenFactory.create(u);
                        } else {
                            authentication = usernamePasswordAuthenticationTokenFactory.create(u);
                        }
                        context.setAuthentication(authentication);
                    }
                }
            } catch (StringIndexOutOfBoundsException e) {
                logger.error(e.getMessage());
            }

        }
        chain.doFilter(request, response);
    }

    public static PublicKey getKey(String key) {

        System.out.println(key);
        try {

            byte[] decoded = Base64.decodeBase64(key);

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate cert = cf.generateCertificate(new ByteArrayInputStream(decoded));
            System.out.println(cert);

            return cert.getPublicKey();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}
