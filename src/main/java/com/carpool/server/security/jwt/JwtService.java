package com.carpool.server.security.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.io.ByteArrayInputStream;
import java.security.Key;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import java.io.*;
import java.nio.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.*;

import java.util.Date;
import java.util.logging.Level;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;

@Component
public class JwtService {

    static final Logger LOGGER = LoggerFactory.getLogger(JwtService.class);

    public String createToken(String username, String secret, Date expireAt) {
        if (StringUtils.hasText(username) && expireAt != null && expireAt.after(new Date())) {

            Algorithm algorithm = Algorithm.RSA256(getPublicKey(), getPrivateKey());
            try {

                String token = JWT.create()
                        .withIssuer("rideshare_middleware")
                        .withExpiresAt(expireAt)
                        .withSubject(username)
                        .sign(algorithm);

                return token;

            } catch (JWTCreationException exception) {
                //Invalid Signing configuration / Couldn't convert Claims.
                LOGGER.error(exception.getMessage());
            }

        }
        return null;
    }

    public boolean isValid(String token, String secret) {
        if (StringUtils.hasText(token) && StringUtils.hasText(secret)) {

            Algorithm algorithm = Algorithm.RSA256(getPublicKey(), getPrivateKey());
            try {

                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("rideshare_middleware")
                        .build(); //Reusable verifier instance
                DecodedJWT jwt = verifier.verify(token);
                if (StringUtils.hasText(jwt.getSubject())) {
                    return true;
                }

            } catch (JWTCreationException exception) {
                //Invalid Signing configuration / Couldn't convert Claims.
                System.out.println(exception.getCause());
            }

        }
        return false;
    }

    public String getUsername(String token, String secret) {
        if (StringUtils.hasText(token) && StringUtils.hasText(secret)) {
            Algorithm algorithm = Algorithm.RSA256(getPublicKey(), getPrivateKey());
            try {

                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("rideshare_middleware")
                        .build(); //Reusable verifier instance
                DecodedJWT jwt = verifier.verify(token);
                return jwt.getSubject();

            } catch (JWTCreationException exception) {
                //Invalid Signing configuration / Couldn't convert Claims.
                System.out.println(exception.getCause());
            }
        }
        return null;
    }

    public static KeyStore getKeyStore() {
        try {
            String filename = "keys/server.jks";

            InputStream in;
            in = JwtService.class.getClassLoader()
                    .getResourceAsStream(filename);

            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(in, "Y%@JKT5".toCharArray());

            return keystore;

        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
            java.util.logging.Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        return null;
    }

    public static RSAPublicKey getPublicKey() {

        try {
            Certificate cert = getKeyStore().getCertificate("my-key-alias");

            PublicKey publicKey = cert.getPublicKey();
            return (RSAPublicKey) publicKey;
        } catch (KeyStoreException ex) {
            java.util.logging.Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        return null;

    }

    public static RSAPrivateKey getPrivateKey() {

        try {
            Key key = getKeyStore().getKey("my-key-alias", "Y%@JKT5".toCharArray());
            return (RSAPrivateKey) key;
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException ex) {
            java.util.logging.Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
