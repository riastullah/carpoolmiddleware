package com.carpool.server.security.factory;


import com.carpool.server.model.User;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import org.springframework.security.authentication.RememberMeAuthenticationToken;

@Component
public class RememberMeAuthenticationTokenFactory {

    public RememberMeAuthenticationToken create(User u) {
        SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(u.getRole());
        RememberMeAuthenticationToken authentication = new RememberMeAuthenticationToken(u.getToken(), u.getUsername(), Arrays.asList(simpleGrantedAuthority));
        return authentication;
    }

}
