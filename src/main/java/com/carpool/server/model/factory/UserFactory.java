package com.carpool.server.model.factory;

import com.carpool.server.model.User;
import org.springframework.stereotype.Component;



@Component
public class UserFactory {

    public User create(String username, String password, String salt, String role) {
        return new User(username, password, salt, role);
    }

}
