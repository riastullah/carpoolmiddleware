package com.carpool.server.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.carpool.server.security.jwt.JwtService;
import java.io.IOException;
import java.io.InputStream;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.logging.Level;

@RestController
public class HelloController {

    // TODO este controller nao tem sentido existir
    @RequestMapping("/")
    public String index() throws UnsupportedEncodingException {

        RSAPublicKey publicKey = (RSAPublicKey) getPublicKey(getKeyStore());
        RSAPrivateKey privateKey = (RSAPrivateKey) getPrivateKey(getKeyStore());
        Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
        try {

            String token = JWT.create()
                    .withIssuer("auth0")
                    .withSubject("roast")
                    .sign(algorithm);
            
            System.out.println(token);

            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("auth0")
                    .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJyaWFzdCIsImlzcyI6ImF1dGgwIn0.omv10erdlDrXRcRaUS9NSwOX99I8pQHGJAXrEpJUjaRnL5Nu3YyTge6wITBOjpKekBX7U_zht5FvrZqMDnIYwoF8xKoVgc9eGaQhoJ7BI96pZ17L-wcQyamkvaJeAmCjahCrWSP7BSm7QS_LtIzqFDvoNVjQ9N7Z94xBBwRVwP2k3Sv5ayGmYQxD3rkS3XhMxeKnUbBrLH7r_LIxpK5xCBW7XzUJFUV4fueR5r8JNiiiVrdAyP6UUkzt0ITTb_ZzWzAyPMweat_X_wDLGxvI82IDHg-Ml3wnEA9vwpAx89zIliIb13puyDtgi3nRmbWeyhXHO4wkEh-i9Kxocf1O3w");
            
            System.out.println(jwt.getSubject());

        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Couldn't convert Claims.
            System.out.println(exception.getCause());
        }
        return "It is working!";
    }
    
    public static KeyStore getKeyStore() {
        try {
            String filename = "keys/server.jks";

            InputStream in;
            in = JwtService.class.getClassLoader()
                    .getResourceAsStream(filename);

            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
            keystore.load(in, "Y%@JKT5".toCharArray());

            return keystore;

        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException ex) {
            java.util.logging.Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        return null;
    }

    public static PublicKey getPublicKey(KeyStore keystore) {

        try {
            Certificate cert = keystore.getCertificate("my-key-alias");

            PublicKey publicKey = cert.getPublicKey();
            return publicKey;
        } catch (KeyStoreException ex) {
            java.util.logging.Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, ex.getMessage());
        }
        return null;

    }

    public static Key getPrivateKey(KeyStore keystore) {

        try {
            Key key = keystore.getKey("my-key-alias", "Y%@JKT5".toCharArray());
            return key;
        } catch (KeyStoreException | NoSuchAlgorithmException | UnrecoverableKeyException ex) {
            java.util.logging.Logger.getLogger(JwtService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
