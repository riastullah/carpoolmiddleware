package com.carpool.server.service;

import com.carpool.server.model.User;
import com.carpool.server.model.factory.UserFactory;
import com.carpool.server.repository.UserRepository;
import com.carpool.server.security.jwt.JwtService;
import com.carpool.server.support.DateGenerator;
import com.carpool.server.support.StringSupport;
import java.io.ByteArrayInputStream;
import java.security.Key;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class UserService {

    @Autowired
    UserRepository userRepository;
    @Autowired
    ShaPasswordEncoder shaPasswordEncoder;
    @Autowired
    StringSupport stringSupport;
    @Autowired
    UserFactory userFactory;
    @Autowired
    JwtService jwtService;
    @Autowired
    DateGenerator dateGenerator;

    public static final String ADAL = "adal";
    public static final String TANENT_ID = "0f9e35db-544f-4f60-bdcc-5ea416e6dc70";
    public static final String USER_EMAIL = "upn";

    static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    
 

    public void create(String username, String password, String role) {
        String salt = stringSupport.generate();
        User u = userFactory.create(username, shaPasswordEncoder.encodePassword(password, salt), salt, role);
        userRepository.save(u);
    }

    public User isLoginValid(String username, String pass) {
        if (!StringUtils.hasText(username) || !StringUtils.hasText(pass)) {
            return null;
        }
        String password = new String(Base64.decodeBase64(pass.getBytes()));
        User u = userRepository.findByUsername(username);
        if (u == null) {
            return null;
        }
        if (!u.getPassword().equals(shaPasswordEncoder.encodePassword(password, u.getSalt()))) {
            return null;
        }
        return u;
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User createUserToken(String username, String secret) {
        String token = jwtService.createToken(username, secret, dateGenerator.getExpirationDate());
        User u = userRepository.findByUsername(username);
        u.setToken(token);
        return userRepository.save(u);
    }

    public User validateUser(String token, String secret, String source) {

        if (StringUtils.hasText(token) && StringUtils.hasText(source) && source.equals(ADAL)) {
            System.out.println("doit");
            //adal login may require creation of user.

//            try {
//                Claims body = Jwts.parser().setSigningKeyResolver(signingKeyResolver).parseClaimsJws(token).getBody();
//                String username = body.getSubject();
//                if (username != null) {
//                    User user = userRepository.findByUsername(username);
//                    if (user != null) {
//                        return user;
//                    } else {
//                        //user has  to be created.                
//                        User u = userFactory.create(username, "", "", "USER");
//                        u.setToken(token);
//                        userRepository.save(u);
//                        User foundUser = userRepository.findByUsername(username);
//                        return foundUser;
//                    }
//                }
//
//            } catch (JwtException e) {
//                LOGGER.error(e.getMessage());
//            }

        } else {
            String username = jwtService.getUsername(token, secret);
            if (username != null) {
                User user = userRepository.findByUsername(username);
                if (user != null && token.equals(user.getToken()) && jwtService.isValid(token, secret)) {
                    return user;
                }
            }
        }
        return null;
    }

    public static PublicKey getAdalPublicKey() {

        String keyString = "MIIC/zCCAeegAwIBAgIBATANBgkqhkiG9w0BAQUFADAaMQswCQYDVQQGEwJVUzEL" +
"MAkGA1UECgwCWjQwHhcNMTMwODI4MTgyODM0WhcNMjMwODI4MTgyODM0WjAaMQsw" +
"CQYDVQQGEwJVUzELMAkGA1UECgwCWjQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAw" +
"ggEKAoIBAQDfdOqotHd55SYO0dLz2oXengw/tZ+q3ZmOPeVmMuOMIYO/Cv1wk2U0" +
"OK4pug4OBSJPhl09Zs6IwB8NwPOU7EDTgMOcQUYB/6QNCI1J7Zm2oLtuchzz4pIb" +
"+o4ZAhVprLhRyvqi8OTKQ7kfGfs5Tuwmn1M/0fQkfzMxADpjOKNgf0uy6lN6utjd" +
"TrPKKFUQNdc6/Ty8EeTnQEwUlsT2LAXCfEKxTn5RlRljDztS7Sfgs8VL0FPy1Qi8" +
"B+dFcgRYKFrcpsVaZ1lBmXKsXDRu5QR/Rg3f9DRq4GR1sNH8RLY9uApMl2SNz+sR" +
"4zRPG85R/se5Q06Gu0BUQ3UPm67ETVZLAgMBAAGjUDBOMB0GA1UdDgQWBBQHZPTE" +
"yQVu/0I/3QWhlTyW7WoTzTAfBgNVHSMEGDAWgBQHZPTEyQVu/0I/3QWhlTyW7WoT" +
"zTAMBgNVHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQDHxqJ9y8alTH7agVMW" +
"Zfic/RbrdvHwyq+IOrgDToqyo0w+IZ6BCn9vjv5iuhqu4ForOWDAFpQKZW0DLBJE" +
"Qy/7/0+9pk2DPhK1XzdOovlSrkRt+GcEpGnUXnzACXDBbO0+Wrk+hcjEkQRRK1bW" +
"2rknARIEJG9GS+pShP9Bq/0BmNsMepdNcBa0z3a5B0fzFyCQoUlX6RTqxRw1h1Qt" +
"5F00pfsp7SjXVIvYcewHaNASbto1n5hrSz1VY9hLba11ivL1N4WoWbmzAL6BWabs" +
"C2D/MenST2/X6hTKyGXpg3Eg2h3iLvUtwcNny0hRKstc73Jl9xR3qXfXKJH0ThTl" +
"q0gq";

        try {
            byte[] decoded = Base64.decodeBase64(keyString);
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            X509Certificate  cert = (X509Certificate)cf.generateCertificate(new ByteArrayInputStream(decoded));
            System.out.println(cert);

            return cert.getPublicKey();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
