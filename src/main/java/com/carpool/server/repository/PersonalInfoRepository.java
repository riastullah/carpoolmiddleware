/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carpool.server.repository;

import com.carpool.server.model.PersonalInfo;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author riast
 */
public interface PersonalInfoRepository extends CrudRepository<PersonalInfo, Long> {
    
}
