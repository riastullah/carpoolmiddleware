/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.carpool.server.repository;

import com.carpool.server.model.User;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

/**
 *
 * @author Riast
 */
public interface UserRepository extends CrudRepository<User, Long> {
   
    public List<User> findAll();
    
    User findByUsername(String username);
    
}
