package com.carpool.server.model;

import com.carpool.server.model.User;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class UserTest {



    @Test
    public void testCreateNonEmptyConstructor() {
        User u = new User("username", "pass", "salt", "USER");
        assertEquals("pass", u.getPassword());
        assertEquals("username", u.getUsername());
        assertEquals("salt", u.getSalt());
        assertEquals(u.getRole(), "USER");
    }


}
